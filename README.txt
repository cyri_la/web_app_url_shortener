This project contains code for easy url shortener
used technologies:
- python + flask framework
for frontend - using bootstrap:
- html
- css
- javascript

How it works:
example of web site:
https://bitly.com/
longer url
https://www.youtube.com/watch?v=DBXq8ijk07s
is shortened to something like 
https://youtu.be/DBXq8ijk07s
