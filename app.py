
from flask import Flask, render_template, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
import string, random

app = Flask(__name__)
#app configurations to bind db
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///urls.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
#creating model for database using sqlalchemy
class Urls(db.Model):
    id_ = db.Column("id_", db.Integer, primary_key = True)
    long_url = db.Column("long_url", db.String())
    #three char limitation for new shorter url
    short_url = db.Column("short_url", db.String(5))

    def __init__(self, long, short):
        self.long_url = long
        self.short_url = short

#creates the db before the first request
@app.before_request
def create_tables():
    db.create_all()
def shorten_url():
    letters = string.ascii_letters;
    while True:
        rand_letters = random.choices(letters, k = 5)
        rand_letters = "".join(rand_letters)
        short_url = Urls.query.filter_by(short_url = rand_letters).first()
        if not short_url:
            print(rand_letters)
            return rand_letters

#decorator function
@app.route('/', methods = ['POST', 'GET'])
def home_page():
    # we got the url in input form
    if request.method == "POST":
        url_received = request.form["urlName"]
        #checking if the url is already in db - if there is not found url: None
        found_url = Urls.query.filter_by(long_url = url_received).first()
        if found_url:
            #return f"{found_url.short_url}"
            # redirects the long url to short url through display
            return redirect(url_for("display_short_url",url = found_url.short_url))
        else:
            short_url = shorten_url()
            new_url = Urls(url_received,short_url)
            db.session.add(new_url)
            db.session.commit()
            #return short_url
            #redirects the short url
            return redirect(url_for("display_short_url", url=short_url))

    else:
        return render_template("home.html")
@app.route('/display/<url>')
def display_short_url(url):
    return render_template("shorturl.html", short_url_display = url)
@app.route('/<short_url>')
def redirection(short_url):
    long_url_row = Urls.query.filter_by(short_url = short_url).first()
    if long_url_row:
        return redirect(long_url_row.long_url)
    else:
        return f'<h1> Url does not exist</h1>'
if __name__ == '__main__':
    app.run(port=5000, debug = True)